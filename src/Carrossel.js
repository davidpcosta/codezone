import React, { Component } from 'react';
import './Carrossel.css';

export default class Carrossel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      currentItem : 1,
      totalItens: 4,
      progress: 0,
      interval: 3000,
      enlapsed: 0
    }

    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.resetCount = this.resetCount.bind(this);
    this.start = this.start.bind(this);
    this.count = this.count.bind(this);
    this.selectItem = this.selectItem.bind(this);

    this.start();
  }

  start() {
    setInterval(() => {
      this.count()
    }, 10);
  }

  count() {
    var enlapsed = this.state.enlapsed + 10;
    if (enlapsed >= this.state.interval) {
      enlapsed = 0;
      this.next();
    }

    var progress = 0;
    if (enlapsed > 0) {
      progress = enlapsed / this.state.interval * 100;
    }

    this.setState({enlapsed: enlapsed, progress: progress});
  }

  resetCount() {
    this.setState({enlapsed: 0});
  }

  selectItem(index) {
    this.setState({currentItem: index});
    this.resetCount();
  }

  prev() {
    console.log('prev clicked');
    var newItem = this.state.currentItem-1;
    if (newItem < 1) {
      newItem = this.state.totalItens;
    }
    this.setState({currentItem: newItem});
    console.log('currentItem : ' + newItem + '/' + this.state.totalItens);
    this.resetCount();
  }

  next() {
    console.log('next clicked');
    var newItem = this.state.currentItem+1;
    if (newItem > this.state.totalItens) {
      newItem = 1;
    }
    this.setState({currentItem: newItem});
    console.log('currentItem : ' + newItem + '/' + this.state.totalItens);
    this.resetCount();
  }

  render() {

    const itens = [];
    for(let i=0; i<4; i++){
      itens.push(<li key={i}  onClick={() => this.selectItem(i+1)} className={this.state.currentItem === (i+1) ? 'done' : ''}>{i+1}</li>)
    }

    return (
      <div className="carrossel">
        <div className="progress-container">
          <div className="progress" style={{width: this.state.progress + '%'}}></div>
        </div>
        <div className="itens">
          <div className={this.state.currentItem !== 1 ? 'hidden' :  'item item-1'}>02.1</div>
          <div className={this.state.currentItem !== 2 ? 'hidden' :  'item item-2'}>02.2</div>
          <div className={this.state.currentItem !== 3 ? 'hidden' :  'item item-3'}>02.3</div>
          <div className={this.state.currentItem !== 4 ? 'hidden' :  'item item-4'}>02.4</div>
        </div>
        <div className="controls">
          <button className="prev" onClick={this.prev}>&lt;</button>
          <ul className="indicator">
            {itens}
          </ul>
          <button className="next" onClick={this.next}>&gt;</button>
        </div>
      </div>
    );
  }
}