import React from 'react';
import './App.css';
import Carrossel from './Carrossel'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        01
      </header>
      <div className="App-body">
        
        <aside className="App-sidebar">
          <Carrossel></Carrossel>
          <div className="group-2">
            <div>03</div>
          </div>
        </aside>

        <main className="App-main">
            <div className="line-1">
              <div className="item">04</div>
              <div className="item">05</div>
            </div>
            <div className="line-2">
              <div className="item">06</div>
            </div>
            <div className="line-3">
              <div className="item">07</div>
              <div className="item">08</div>
              <div className="item">09</div>
            </div>
        </main>
      </div>
      <footer className="App-footer">
        10
      </footer>
    </div>
  );
}

export default App;
